#!/bin/sh

usage() {
    echo "USAGE: ./setup.sh <directory> [up|down]"
    echo "=> example: ./setup.sh nextcloud up"
}

# check if there are exactly two arguments
if [ $# != 2 ]; then
    echo "wrong number of arguments"
    usage
    exit 1
fi

directory="$1"
action="$2"

# check if the diven directory exists
if [ ! -d $directory ]; then
    echo "directory does not exist: ${directory}"
    usage
    exit 1
fi

# handle docker compose actions
case $action in
"up") docker compose --file "${directory}/docker-compose.yml" --env-file .env up -d ;;
"down") docker compose --file "${directory}/docker-compose.yml" --env-file .env down ;;
"restart") {
    docker compose --file "${directory}/docker-compose.yml" --env-file .env down
    docker compose --file "${directory}/docker-compose.yml" --env-file .env up -d
} ;;
*) {
    echo "unknown action: $action"
    usage
    exit 1
} ;;
esac
